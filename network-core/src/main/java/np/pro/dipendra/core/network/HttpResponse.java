package np.pro.dipendra.core.network;

public interface HttpResponse<T> {

  int getHttpCode();

  boolean isSuccess();

  String getMessage();

  T getResponseObject();

  String getRawResponse();

  String getHeader(String headerName);
}
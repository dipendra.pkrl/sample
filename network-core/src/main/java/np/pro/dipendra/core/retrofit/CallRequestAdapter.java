package np.pro.dipendra.core.retrofit;

import android.content.Context;

import np.pro.dipendra.core.network.RequestCall;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;

/**
 * This is a {@link CallAdapter.Factory} that adapts {@link Call} to {@link RequestCall} using the wrapper {@link RetrofitCallImpl}
 */
public class CallRequestAdapter extends CallAdapter.Factory {
    private Context mContext;

    public CallRequestAdapter(Context context){
        mContext = context;
    }

    @Override
    public CallAdapter<?, ?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
      if (!(returnType instanceof ParameterizedType))
        return null;
      ParameterizedType returnClass = (ParameterizedType) returnType;
      if (returnClass.getRawType() != RequestCall.class) {
        return null;
      }
      Type[] types = returnClass.getActualTypeArguments();
      if (types.length != 1) {
        return null;
      }

      final Type objType = types[0];

      return new CallAdapter<Object, Object>() {
        @Override
        public Type responseType() {
          return objType;
        }

        @Override
        public Object adapt(Call call) {
          return new RetrofitCallImpl(mContext, call);
        }
      };
    }

}

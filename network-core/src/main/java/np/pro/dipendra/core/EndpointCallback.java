package np.pro.dipendra.core;

import np.pro.dipendra.core.network.HttpResponse;
import np.pro.dipendra.core.network.RequestCall;

public interface EndpointCallback<T> {
  void onResponse(RequestCall<T> call, HttpResponse<T> response);

  void onFailure(RequestCall<T> call, Throwable throwable);
}
package np.pro.dipendra.core.network;

import np.pro.dipendra.core.Error;

public interface Callback<T> {
  void onResponse(HttpResponse<T> response);

  void onFailure(Error Error);

  boolean runOnUiThread();
}
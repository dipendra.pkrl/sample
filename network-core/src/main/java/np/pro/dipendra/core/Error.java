package np.pro.dipendra.core;

public class Error {
  public static final int GENERAL_ERROR = 9001;
  public static final int SERVER_ERROR_5XX = 9002;
  public static final int EMPTY_OBJECT_RESPONSE = 9003;
  public static final int PARSING_EXCEPTION = 9004;
  public static final int IO_EXCEPTION = 9005;
  public static final int ERROR_FOLDER_ALREADY_EXIST = 9006;
  public static final int ERROR_DATE_TOO_OLD = 9007;
  public static final int ERROR_DATE_TOO_NEW = 9008;

  private long mErrorCode;
  private String mErrorMsg;

  public Error(int errorCode, String errorMsg) {
    mErrorCode = errorCode;
    mErrorMsg = errorMsg;
  }

  public long getCode() {
    return mErrorCode;
  }

  public String getMessage() {
    return mErrorMsg;
  }
}
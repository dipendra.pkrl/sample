package np.pro.dipendra.core.api

import np.pro.dipendra.core.model.response.UserListResponse
import np.pro.dipendra.core.network.RequestCall
import retrofit2.http.GET
import retrofit2.http.Query

interface UsersApi {
    @GET("/api/1.1/")
    fun getUserList(@Query("page") page: Int, @Query("results") noOfResultItems: Int): RequestCall<UserListResponse>
}

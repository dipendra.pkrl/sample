package np.pro.dipendra.core.network;

import java.net.URL;
import java.util.List;
import java.util.Map;

public interface HttpRequest<T> {
    Map<String, List<String>> getHeaders();
    URL getUrl();
    String getMethod();
    boolean isHttps();
}
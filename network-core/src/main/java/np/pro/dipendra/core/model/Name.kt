package np.pro.dipendra.core.model

import org.apache.commons.lang3.StringUtils
import java.io.Serializable
import java.util.*

data class Name(val title: String, val first: String, val last: String):Serializable {
    val fullName: String
        get() {
            return StringUtils.join(Arrays.asList(title, first, last), StringUtils.SPACE)
        }
}

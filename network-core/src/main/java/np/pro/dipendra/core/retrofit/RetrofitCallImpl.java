package np.pro.dipendra.core.retrofit;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import np.pro.dipendra.core.Error;
import np.pro.dipendra.core.network.Callback;
import np.pro.dipendra.core.network.HttpRequest;
import np.pro.dipendra.core.network.HttpResponse;
import np.pro.dipendra.core.network.RequestCall;

import org.apache.commons.lang3.Range;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

public class RetrofitCallImpl<T> implements RequestCall {
  private Call<T> mCall;
  private Context mContext;

  public RetrofitCallImpl(Context context, Call<T> call) {
    mContext = context.getApplicationContext();
    mCall = call;
  }

  @Override
  public HttpResponse<T> execute() throws IOException {
    return new RetrofitResponseImpl(mCall.execute());
  }

  private static boolean is5XXServerError(int code) {
    Range<Integer> myRange = Range.between(500, 599);
    return myRange.contains(code);
  }

  @Override
  public void enqueue(final Callback callback) {

    mCall.enqueue(new retrofit2.Callback<T>() {
      @Override
      public void onResponse(Call<T> call, final Response<T> response) {
        if (response == null) {
          sendOnFailureCallback(new Error(Error.EMPTY_OBJECT_RESPONSE, "Response Object is null"), callback);
          return;
        }

        if (is5XXServerError(response.code())) {
          sendOnFailureCallback(new Error(Error.SERVER_ERROR_5XX, mContext.getString(np.pro.dipendra.core.R.string.sb__http_server_error)), callback);
          return;
        }

        if (!response.isSuccessful()) {
          sendOnFailureCallback(new Error(Error.GENERAL_ERROR, "Response was unsuccessful"), callback);
          return;
        }

        if (callback.runOnUiThread()) {
          getMainLoopHandler().post(new Runnable() {
            @Override
            public void run() {
              callback.onResponse(new RetrofitResponseImpl(response));
            }
          });
        } else {
          callback.onResponse(new RetrofitResponseImpl(response));
        }
      }

      private void sendOnFailureCallback(final Error error, final Callback callback) {
        if (callback.runOnUiThread()) {
          getMainLoopHandler().post(new Runnable() {
            @Override
            public void run() {
              callback.onFailure(error);
            }
          });
        } else {
          callback.onFailure(error);
        }
      }

      @Override
      public void onFailure(Call<T> call, final Throwable t) {
        sendOnFailureCallback(new Error(Error.GENERAL_ERROR, t.getMessage()), callback);
      }
    });
  }

  private static Handler getMainLoopHandler() {
    return new Handler(Looper.getMainLooper());
  }

  @Override
  public boolean isExecuted() {
    return mCall.isExecuted();
  }

  @Override
  public void cancel() {
    mCall.cancel();
  }

  @Override
  public boolean isCanceled() {
    return mCall.isCanceled();
  }

  @Override
  public RequestCall<T> clone() {
    return new RetrofitCallImpl(mContext, mCall.clone());
  }

  @Override
  public HttpRequest request() {
    return new OkHttpRequestImpl(mCall.request());
  }

  @Override
  public String toString() {
    return RetrofitCallImpl.class.getCanonicalName() + "<" + mCall.toString() + ">";
  }
}
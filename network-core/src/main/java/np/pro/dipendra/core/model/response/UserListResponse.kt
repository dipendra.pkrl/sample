package np.pro.dipendra.core.model.response

import np.pro.dipendra.core.model.Info
import np.pro.dipendra.core.model.User

data class UserListResponse(val results: List<User>, val info: Info)
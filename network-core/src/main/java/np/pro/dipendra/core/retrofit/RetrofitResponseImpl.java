package np.pro.dipendra.core.retrofit;

import np.pro.dipendra.core.network.HttpResponse;

import java.io.IOException;

import retrofit2.Response;

public class RetrofitResponseImpl<T> implements HttpResponse {
  private Response<T> mResponse;

  public RetrofitResponseImpl(Response<T> response) {
    mResponse = response;
  }

  public int getHttpCode() {
    return mResponse.code();
  }

  public boolean isSuccess() {
    return mResponse.isSuccessful();
  }

  public String getMessage() {
    return mResponse.message();
  }

  public T getResponseObject() {
    return mResponse.body();
  }

  @Override
  public String getRawResponse() {
    try {
      return mResponse.raw().body().string();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public String getHeader(String headerName) {
    return mResponse.headers().get(headerName);
  }

  @Override
  public String toString() {
    return RetrofitResponseImpl.class.getCanonicalName() + "<" + mResponse.toString() + ">";
  }
}
package np.pro.dipendra.core.retrofit;

import np.pro.dipendra.core.network.HttpRequest;

import java.net.URL;
import java.util.List;
import java.util.Map;

import okhttp3.Request;

public class OkHttpRequestImpl implements HttpRequest {
    private Request mRequest;
    public OkHttpRequestImpl(Request request){
        mRequest = request;
    }

    @Override
    public Map<String, List<String>> getHeaders() {
        return mRequest.headers().toMultimap();
    }

    @Override
    public URL getUrl() {
        return mRequest.url().url();
    }

    @Override
    public String getMethod() {
        return mRequest.method();
    }

    @Override
    public boolean isHttps() {
        return mRequest.isHttps();

    }

    public String getContentType() {
        return mRequest.body().contentType().toString();
    }
}
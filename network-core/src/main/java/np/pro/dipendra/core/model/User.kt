package np.pro.dipendra.core.model

import java.io.Serializable

data class User(val gender: String, val email: String, val phone: String, val name: Name, val location: Location, val picture: Picture) : Serializable
package np.pro.dipendra.core;

import np.pro.dipendra.core.network.Callback;

public abstract class UiCallback<T> implements Callback<T> {
  public boolean runOnUiThread() {
    return true;
  }
}
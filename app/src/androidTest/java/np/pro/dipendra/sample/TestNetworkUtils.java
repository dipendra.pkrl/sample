package np.pro.dipendra.sample;

import com.google.gson.Gson;

import java.io.IOException;

import np.pro.dipendra.core.network.Callback;
import np.pro.dipendra.core.network.HttpRequest;
import np.pro.dipendra.core.network.HttpResponse;
import np.pro.dipendra.core.network.RequestCall;

/**
 * Created by dipendrapokharel on 2017-04-19.
 */

public class TestNetworkUtils {

  public static <T> RequestCall<T> getRequestCall(final Class<T> t, final String expectedResponseText) {
    RequestCall<T> requestCall = new RequestCall<T>() {
      @Override
      public HttpResponse<T> execute() throws IOException {
        return getHttpResponse(t, expectedResponseText);
      }

      @Override
      public void enqueue(Callback<T> callback) {
        callback.onResponse(getHttpResponse(t, expectedResponseText));
      }

      @Override
      public boolean isExecuted() {
        return false;
      }

      @Override
      public void cancel() {

      }

      @Override
      public boolean isCanceled() {
        return false;
      }

      @Override
      public RequestCall<T> clone() {
        return null;
      }

      @Override
      public HttpRequest request() {
        return null;
      }
    };
    return requestCall;
  }

  public static <T> HttpResponse<T> getHttpResponse(final Class<T> t, final String expectedResponseText) {
    return new HttpResponse<T>() {

      @Override
      public int getHttpCode() {
        return 0;
      }

      @Override
      public boolean isSuccess() {
        return false;
      }

      @Override
      public String getMessage() {
        return null;
      }

      @Override
      public T getResponseObject() {
        return new Gson().fromJson(expectedResponseText, t);
      }

      @Override
      public String getRawResponse() {
        return null;
      }

      @Override
      public String getHeader(String headerName) {
        return null;
      }
    };
  }

}

package np.pro.dipendra.sample

import android.support.test.runner.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import org.skyscreamer.jsonassert.JSONAssert


@RunWith(AndroidJUnit4::class)
class JsonAssertTest {
    @Test
    @Throws(Exception::class)
    fun testGetUserList() {
        JSONAssert.assertEquals("{\n" +
                "  \"array\": [\n" +
                "    1,\n" +
                "    2,\n" +
                "    3\n" +
                "  ]}",

                "{\n" +
                        "  \"array\": [\n" +
                        "    1,\n" +
                        "    2,\n" +
                        "    3\n" +
                        "  ]}", false)


        JSONAssert.assertEquals("{\n" +
                "  \"array\": [\n" +
                "    1,\n" +
                "    2,\n" +
                "    3\n" +
                "  ]}",

                "{\n" +
                        "  \"array\": [\n" +
                        "    3,\n" +
                        "    2,\n" +
                        "    1\n" +
                        "  ]}", false)
    }
}

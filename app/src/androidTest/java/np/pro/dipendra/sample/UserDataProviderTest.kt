package np.pro.dipendra.sample

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.google.gson.Gson
import np.pro.dipendra.core.Error
import np.pro.dipendra.core.api.UsersApi
import np.pro.dipendra.core.model.response.UserListResponse
import np.pro.dipendra.core.network.RequestCall
import np.pro.dipendra.sample.modules.DataProviderCallback
import np.pro.dipendra.sample.modules.impl.UserDataProviderImpl
import org.apache.commons.io.IOUtils
import org.json.JSONException
import org.junit.Test
import org.junit.runner.RunWith
import org.skyscreamer.jsonassert.JSONAssert
import retrofit2.http.Query
import java.util.concurrent.CountDownLatch


@RunWith(AndroidJUnit4::class)
class UserDataProviderTest {

    @Test
    @Throws(Exception::class)
    fun testGetUserList() {
        val sampleResponse = IOUtils.toString(InstrumentationRegistry.getContext().resources.openRawResource(np.pro.dipendra.sample.test.R.raw.user_list))
        val sampleUserListResponse = Gson().fromJson(sampleResponse, UserListResponse::class.java)

        val userDataProvider = UserDataProviderImpl(object : UsersApi {
            override fun getUserList(@Query("page") page: Int, @Query("results") noOfResultItems: Int): RequestCall<UserListResponse> {
                return TestNetworkUtils.getRequestCall(UserListResponse::class.java, sampleResponse)
            }
        })
        val countDownLatch = CountDownLatch(1)
        userDataProvider.getUserList(1, object : DataProviderCallback<UserListResponse> {
            override fun onRetrieved(retrievedObject: UserListResponse) {
                val retrievedJson = Gson().toJsonTree(retrievedObject)

                val expectedUserListResponseString = Gson().toJson(sampleUserListResponse)

                try {
                    JSONAssert.assertEquals(retrievedJson.toString(), expectedUserListResponseString, false)
                } catch (e: JSONException) {
                    AssertionError(e)
                }

                countDownLatch.countDown()
            }

            override fun onFailed(error: Error) {
                AssertionError(error.message)
                countDownLatch.countDown()
            }
        })
        countDownLatch.await()
    }
}

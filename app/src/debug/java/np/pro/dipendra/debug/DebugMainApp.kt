package np.pro.dipendra.sample

import com.facebook.stetho.Stetho

class DebugMainApp : MainApp() {
    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }
}
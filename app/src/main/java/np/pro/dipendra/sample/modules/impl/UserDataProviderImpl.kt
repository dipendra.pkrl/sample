package np.pro.dipendra.sample.modules.impl

import np.pro.dipendra.core.api.UsersApi
import np.pro.dipendra.core.model.response.UserListResponse
import np.pro.dipendra.sample.modules.DataProviderCallback
import np.pro.dipendra.sample.modules.DataProviderUtils
import np.pro.dipendra.sample.modules.interfaces.UserDataProvider

class UserDataProviderImpl(private val mUsersApi: UsersApi) : UserDataProvider {
    override fun getUserList(pageNumber: Int, listener: DataProviderCallback<UserListResponse>) {
        DataProviderUtils.queueAndUiNotify(mUsersApi.getUserList(pageNumber, 60), listener)
    }
}

package np.pro.dipendra.sample.modules.interfaces

import np.pro.dipendra.core.model.response.UserListResponse
import np.pro.dipendra.sample.modules.DataProviderCallback

interface UserDataProvider {
    fun getUserList(pageNumber: Int, listener: DataProviderCallback<UserListResponse>)
}
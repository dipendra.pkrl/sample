package np.pro.dipendra.iptv.modules.interfaces


import okhttp3.OkHttpClient

interface OkHttp {
    fun okHttpClient(): OkHttpClient
}

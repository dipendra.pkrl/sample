package np.pro.dipendra.sample.modules;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.support.v4.app.Fragment;

import java.lang.ref.WeakReference;

import np.pro.dipendra.core.Error;
import np.pro.dipendra.core.UiCallback;
import np.pro.dipendra.core.network.HttpResponse;
import np.pro.dipendra.core.network.RequestCall;

public class DataProviderUtils {

  public static void queueAndUiNotify(RequestCall requestCall, final DataProviderCallback dataProviderCallback) {
    requestCall.enqueue(new UiCallback() {
      @Override
      public void onResponse(HttpResponse response) {
        if (dataProviderCallback != null) {
          dataProviderCallback.onRetrieved(response.getResponseObject());
        }

      }

      @Override
      public void onFailure(Error error) {
        if (dataProviderCallback != null) {
          dataProviderCallback.onFailed(error);
        }
      }
    });
  }




  public static boolean isUiSafe(MODE mode, WeakReference<Context> contextWeakReference, WeakReference<Fragment> fragmentWeakReference) {
    if (mode == MODE.CONTEXT) {
      Context context = contextWeakReference == null ? null : contextWeakReference.get();
      if (context == null) {
        return false;
      } else if (context instanceof Activity) {
        Activity activity = (Activity) context;
        if (!activity.isFinishing()) {
          return true;
        }
      } else if (context instanceof Service || context instanceof Application) {
        return true;
      }
      return false;
    } else if (mode == MODE.FRAGMENT) {
      Fragment fragment = fragmentWeakReference.get();
      if (fragment != null && fragment.isAdded() && fragment.isVisible()) {
        return true;
      } else {
        return false;
      }
    } else {
      throw new IllegalStateException("Unknown  " + MODE.class.getCanonicalName());
    }
  }

  public enum MODE {
    FRAGMENT, CONTEXT
  }
}

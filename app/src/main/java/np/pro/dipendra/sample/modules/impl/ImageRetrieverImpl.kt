package np.pro.dipendra.sample.modules.impl

import android.widget.ImageView
import com.squareup.picasso.Picasso
import np.pro.dipendra.sample.R
import np.pro.dipendra.sample.modules.interfaces.ImageRetriever

class ImageRetrieverImpl(private val mPicasso: Picasso) : ImageRetriever {

    override fun loadImage(imageView: ImageView, url: String) {
        mPicasso.load(url).placeholder(R.drawable.profile).into(imageView)
    }
}
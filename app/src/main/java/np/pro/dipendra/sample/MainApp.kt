package np.pro.dipendra.sample

import android.app.Application
import np.pro.dipendra.iptv.dagger.DaggerWrapper

open class MainApp : Application() {
     override fun onCreate() {
        super.onCreate()
        DaggerWrapper.init(this)
        DaggerWrapper.mComponent.inject(this)
    }
}
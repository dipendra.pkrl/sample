package np.pro.dipendra.sample.modules.impl

import np.pro.dipendra.sample.modules.interfaces.Environment
import okhttp3.HttpUrl

enum class DefaultEnvironment constructor(private val mHostName: String, private val mHasHttps: Boolean = true) : Environment {
    TESTING("testing.randomuser.me"),
    BETA_STAGING("randomuser.me");

    override fun getHostName(): String {
        return mHostName
    }

    override fun hasHttps(): Boolean {
        return mHasHttps
    }

    override fun getBaseUrl(): String {
        val builder = HttpUrl.Builder()
        if (mHasHttps) {
            builder.scheme("https")
        } else {
            builder.scheme("http")
        }
        return builder.host(mHostName).build().toString()
//        return builder.host(mHostName).encodedPath("/api/1.1/").build().toString()
    }
}
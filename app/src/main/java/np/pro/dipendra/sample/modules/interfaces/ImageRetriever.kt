package np.pro.dipendra.sample.modules.interfaces

import android.widget.ImageView
import org.jetbrains.annotations.NotNull

interface ImageRetriever {
    fun loadImage(imageView: ImageView, @NotNull url: String)
}
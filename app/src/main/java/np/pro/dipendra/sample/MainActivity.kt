package np.pro.dipendra.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.ViewFlipper
import kotlinx.android.synthetic.main.activity_main.*
import np.pro.dipendra.core.Error
import np.pro.dipendra.core.model.User
import np.pro.dipendra.core.model.response.UserListResponse
import np.pro.dipendra.iptv.dagger.DaggerWrapper
import np.pro.dipendra.sample.modules.UiSafeDataProviderCallback
import np.pro.dipendra.sample.modules.interfaces.ImageRetriever
import np.pro.dipendra.sample.modules.interfaces.UserDataProvider
import javax.inject.Inject

class MainActivity : AppCompatActivity(), UserListAdapter.UserClickListener {
    private var mPageNumber = 0
    private lateinit var mUserAdapter: UserListAdapter
    private lateinit var mUserList: MutableList<User>

    @Inject lateinit var mUserDataProvider: UserDataProvider
    @Inject lateinit var mImageRetriever: ImageRetriever

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerWrapper.mComponent.inject(this)
        tvError.setOnClickListener { downloadUsers() }
        downloadUsers()
    }

    private fun downloadUsers() {
        userListViewFlipper.showChild(progressBar)
        mUserDataProvider.getUserList(mPageNumber, object : UiSafeDataProviderCallback<UserListResponse>(this) {
            override fun onSafeSuccess(response: UserListResponse) {
                if (mPageNumber == 0) {
                    setupAdapter(response.results)
                } else {
                    updateAdapter(response.results)
                }
            }

            override fun onSafeFailure(error: Error?) {
                if (mPageNumber == 0) {
                    userListViewFlipper.showChild(tvError)
                }
            }
        })
    }

    private fun updateAdapter(results: List<User>) {
        mUserList.addAll(results)
        mUserAdapter.notifyDataSetChanged()
    }

    private fun setupAdapter(results: List<User>) {
        if (results.size == 0) {
            userListViewFlipper.showChild(tvEmpty)
            return
        }

        mUserList = results as MutableList<User>
        mUserAdapter = UserListAdapter(mUserList, mImageRetriever, this@MainActivity)

        userListRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        setRecyclerViewDivider()
        userListRecyclerView.adapter = mUserAdapter
        userListViewFlipper.showChild(userListRecyclerView)
    }

    private fun setRecyclerViewDivider() {
        val dividerItemDecoration = DividerItemDecoration(userListRecyclerView.context,
                (userListRecyclerView.layoutManager as LinearLayoutManager).orientation)
        userListRecyclerView.addItemDecoration(dividerItemDecoration)
    }

    override fun onUserClicked(user: User) {
        startActivity(UserDetailActivity.newIntent(this, user))
    }

    private fun ViewFlipper.showChild(child: View) {
        this.displayedChild = this.indexOfChild(child)
    }

}

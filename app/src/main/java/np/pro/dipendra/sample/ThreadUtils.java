package np.pro.dipendra.sample;

import android.os.Handler;
import android.os.Looper;

public class ThreadUtils {

  public static Handler getUiHandler() {
    return new Handler(Looper.getMainLooper());
  }
}

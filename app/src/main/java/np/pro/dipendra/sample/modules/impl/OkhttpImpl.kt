package np.pro.dipendra.sample.modules.impl

import com.facebook.stetho.okhttp3.StethoInterceptor
import np.pro.dipendra.iptv.modules.interfaces.OkHttp
import np.pro.dipendra.sample.BuildConfig
import okhttp3.OkHttpClient

class OkHttpImpl : OkHttp {

    override fun okHttpClient(): OkHttpClient {
        val httpBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            httpBuilder.networkInterceptors().add(StethoInterceptor())
        }
        return httpBuilder.build()
    }
}
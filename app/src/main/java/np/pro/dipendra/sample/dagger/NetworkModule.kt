package np.pro.dipendra.iptv.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import np.pro.dipendra.core.api.UsersApi
import np.pro.dipendra.core.retrofit.CallRequestAdapter
import np.pro.dipendra.iptv.modules.interfaces.OkHttp
import np.pro.dipendra.sample.modules.impl.OkHttpImpl
import np.pro.dipendra.sample.modules.impl.UserDataProviderImpl
import np.pro.dipendra.sample.modules.interfaces.Environment
import np.pro.dipendra.sample.modules.interfaces.UserDataProvider
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule constructor(private val mEnvironment: Environment) {

    @Provides
    @Singleton
    internal fun providesEnvironment(): Environment {
        return mEnvironment
    }

    @Provides
    @Singleton
    internal fun providesRetrofitBuilder(@AppContext context: Context): Retrofit.Builder {
        return Retrofit.Builder()
                .baseUrl(mEnvironment.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CallRequestAdapter(context))
    }

    @Provides
    @Singleton
    internal fun provideOkhttp(): OkHttp {
        return OkHttpImpl()
    }

    @Provides
    @Singleton
    internal fun provideRetrofit(builder: Retrofit.Builder, okHttp: OkHttp): Retrofit {
        return builder.client(okHttp.okHttpClient()).build()
    }

    @Provides
    @Singleton
    internal fun providesReminderApi(retrofit: Retrofit): UsersApi {
        return retrofit.create(UsersApi::class.java)
    }

    @Provides
    internal fun providesReminderDataProvider(usersApi: UsersApi): UserDataProvider {
        return UserDataProviderImpl(usersApi)
    }
}


package np.pro.dipendra.iptv.dagger

import android.content.Context
import np.pro.dipendra.sample.modules.impl.DefaultEnvironment

object DaggerWrapper {

    lateinit var mComponent: DaggerComponent

    fun init(context: Context) {
        mComponent = DaggerMainComponent
                .builder()
                .contextModule(ContextModule(context))
                .networkModule(NetworkModule(DefaultEnvironment.BETA_STAGING))
                .build()
    }
}
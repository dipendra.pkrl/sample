package np.pro.dipendra.iptv.dagger

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ContextModule::class,NetworkModule::class, MiscModules::class))
interface MainComponent : DaggerComponent
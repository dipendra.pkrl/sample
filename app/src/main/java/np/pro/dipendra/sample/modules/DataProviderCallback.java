package np.pro.dipendra.sample.modules;

import np.pro.dipendra.core.Error;

public interface DataProviderCallback<T> {
  void onRetrieved(T retrievedObject);

  void onFailed(Error error);
}

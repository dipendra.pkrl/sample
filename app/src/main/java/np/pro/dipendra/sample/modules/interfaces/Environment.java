package np.pro.dipendra.sample.modules.interfaces;

public interface Environment {
    String getHostName();
    boolean hasHttps();
    String getBaseUrl();
}

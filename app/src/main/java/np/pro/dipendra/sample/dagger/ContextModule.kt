package np.pro.dipendra.iptv.dagger

import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ContextModule(@AppContext context: Context) {
    private val mContext: Context = context.applicationContext

    @Provides
    @AppContext
    internal fun provideContext(): Context {
        return mContext
    }

}


package np.pro.dipendra.sample

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import kotlinx.android.synthetic.main.user_list_item.view.*
import np.pro.dipendra.core.model.User
import np.pro.dipendra.sample.modules.interfaces.ImageRetriever

class UserListAdapter(private val mUserList: List<User>, private val mImageRetriever: ImageRetriever, private val mUserClickListener: UserClickListener) : RecyclerView.Adapter<UserListAdapter.UserItemViewHolder>() {

    interface UserClickListener {
        fun onUserClicked(user: User)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserItemViewHolder {
        val mItemView = LayoutInflater.from(parent.context).inflate(R.layout.user_list_item, parent, false)
        return UserItemViewHolder(mItemView)
    }

    override fun onBindViewHolder(holder: UserItemViewHolder, position: Int) {
        holder.bindItem(mUserList[position])
    }

    override fun getItemCount(): Int {
        return mUserList.size
    }

    inner class UserItemViewHolder(private val mItemView: View) : RecyclerView.ViewHolder(mItemView), OnClickListener {
        private lateinit var mItem: User

        fun bindItem(user: User) {
            mItem = user
            mItemView.tvName.text = user.name.fullName
            mImageRetriever.loadImage(mItemView.ivProfile, mItem.picture.large)
            mItemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            mUserClickListener?.onUserClicked(mItem)
        }
    }
}

package np.pro.dipendra.sample.utils;

import np.pro.dipendra.core.BuildConfig;

/**
 * Utility methods to print log messages only if the build is DEBUG
 */
public final class Log {

    public static void i(final String msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.i(getFullClassName(), getLogMessage(msg));
        }
    }

    public static void e(final String msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.e(getFullClassName(), getLogMessage(msg));
        }
    }

    public static void e(final Exception msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.e(getFullClassName(), getLogMessage(android.util.Log.getStackTraceString(msg)));
        }
    }

    public static void v(final String msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.v(getFullClassName(), getLogMessage(msg));
        }
    }

    public static void d(final String msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.e(getFullClassName(), getLogMessage(msg));
        }
    }

    /**
    * Attention this `user` visible log it is the only one that will be visible in production builds.
    */
    public static void u(final String msg) {
        android.util.Log.e(getFullClassName(), getLogMessage(msg));
    }

    public static void w(final String msg) {
        if (BuildConfig.DEBUG) {
            android.util.Log.e(getFullClassName(), getLogMessage(msg));
        }
    }

    private static String getLogMessage(String msg){
        return "SBLog:" + getClassName() + "." + getMethodName() + "()#" + getLineNumber() + ":" + msg;
    }

    private static String getFullClassName() {
        return getStackTrace().getClassName();
    }

    private static String getClassName() {
        String fullClassName = getStackTrace().getClassName();
        return fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
    }

    private static String getMethodName(){
        return getStackTrace().getMethodName();
    }

    private static int getLineNumber(){
        return getStackTrace().getLineNumber();
    }

    private static StackTraceElement getStackTrace() {
        // this number at the end of this method depend on how deep is the stacktrace form current thread.
        return Thread.currentThread().getStackTrace()[6];
    }
}
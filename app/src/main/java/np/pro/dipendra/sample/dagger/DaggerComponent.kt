package np.pro.dipendra.iptv.dagger

import np.pro.dipendra.sample.MainActivity
import np.pro.dipendra.sample.MainApp
import np.pro.dipendra.sample.UserDetailActivity

interface DaggerComponent {
    fun inject(mainApp: MainApp)
    fun inject(mainApp: MainActivity)
    fun inject(userDetailActivity: UserDetailActivity)
}

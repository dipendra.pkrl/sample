package np.pro.dipendra.sample

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_detail.*
import np.pro.dipendra.core.model.User
import np.pro.dipendra.iptv.dagger.DaggerWrapper
import np.pro.dipendra.sample.modules.interfaces.ImageRetriever
import java.io.Serializable
import javax.inject.Inject

class UserDetailActivity : AppCompatActivity() {
    companion object {
        private val INTENT_EXTRA_USER = "intent_extra_user"

        fun newIntent(context: Context, user: User): Intent {
            val intent = Intent(context, UserDetailActivity::class.java)
            intent.putExtra(INTENT_EXTRA_USER, user as Serializable)
            return intent
        }
    }

    @Inject
    lateinit var mImageRetriever: ImageRetriever

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerWrapper.mComponent.inject(this)
        setContentView(R.layout.activity_detail)
        init(intent.getSerializableExtra(INTENT_EXTRA_USER) as User)
    }

    private fun init(user: User) {
        mImageRetriever.loadImage(ivProfile, user.picture.large)
        tvName.text = user.name.fullName


        ivAddress.setOnClickListener { Toast.makeText(this@UserDetailActivity, "TODO", Toast.LENGTH_SHORT).show() }
        ivPhone.setOnClickListener {
            Toast.makeText(this@UserDetailActivity, "TODO", Toast.LENGTH_SHORT).show()
//
//            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:${user.phone}"))
//            startActivityForIntent(intent)
        }
        ivEmail.setOnClickListener {
            val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", user.email, null))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Body")
            startActivityForIntent(emailIntent)
        }
    }

    private fun startActivityForIntent(intent: Intent) {
        if (intent.resolveActivity(packageManager) == null) {
            Toast.makeText(this, "No app can perform this action", Toast.LENGTH_SHORT).show()
        } else {
            startActivity(intent)
        }
    }

}


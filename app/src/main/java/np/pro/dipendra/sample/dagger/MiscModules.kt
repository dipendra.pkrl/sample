package np.pro.dipendra.iptv.dagger

import android.content.Context
import android.util.Log
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.Cache
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import np.pro.dipendra.iptv.modules.interfaces.OkHttp
import np.pro.dipendra.sample.BuildConfig
import np.pro.dipendra.sample.modules.impl.ImageRetrieverImpl
import np.pro.dipendra.sample.modules.interfaces.ImageRetriever
import javax.inject.Singleton

@Module
class MiscModules {

    @Provides
    @Singleton
    fun providePicasso(@AppContext context: Context,okHttp: OkHttp): Picasso {
        return Picasso.Builder(context)
                .listener { _, uri, exception -> np.pro.dipendra.sample.utils.Log.e("onImageLoadFailed: " + uri.toString() + " " + Log.getStackTraceString(exception)) }
                .downloader(OkHttp3Downloader(okHttp.okHttpClient()))
                .memoryCache(Cache.NONE)
                .loggingEnabled(BuildConfig.DEBUG)
                .build()
    }
    @Provides
    @Singleton
    fun provideImageRetriever(picasso: Picasso): ImageRetriever {
        return ImageRetrieverImpl(picasso)
    }

}
package np.pro.dipendra.sample.modules;

import android.content.Context;
import android.support.v4.app.Fragment;

import np.pro.dipendra.core.Error;

import java.lang.ref.WeakReference;

import np.pro.dipendra.sample.ThreadUtils;
import np.pro.dipendra.sample.utils.Log;

public abstract class UiSafeDataProviderCallback<T> implements DataProviderCallback<T> {
  private WeakReference<Context> mContextWeakReference;
  private WeakReference<Fragment> mFragmentWeakReference;
  private DataProviderUtils.MODE mMode;

  public UiSafeDataProviderCallback(Context context) {
    mContextWeakReference = new WeakReference<>(context);
    mMode = DataProviderUtils.MODE.CONTEXT;
  }

  public UiSafeDataProviderCallback(Fragment fragment) {
    mFragmentWeakReference = new WeakReference<>(fragment);
    mMode = DataProviderUtils.MODE.FRAGMENT;
  }

  protected abstract void onSafeSuccess(T response);

  protected abstract void onSafeFailure(Error error);

  public final void onRetrieved(final T retrievedObject) {
    if (DataProviderUtils.isUiSafe(mMode, mContextWeakReference, mFragmentWeakReference)) {
      ThreadUtils.getUiHandler().post(new Runnable() {
        @Override
        public void run() {
          onSafeSuccess(retrievedObject);
        }
      });
    } else {
      Log.v("unsafe to return retrieved data");
    }
  }

  @Override
  public final void onFailed(final Error error) {
    if (DataProviderUtils.isUiSafe(mMode, mContextWeakReference, mFragmentWeakReference)) {
      ThreadUtils.getUiHandler().post(new Runnable() {
        @Override
        public void run() {
          onSafeFailure(error);
        }
      });
    } else {
      Log.v("unsafe to call onFailure");
    }
  }
}
